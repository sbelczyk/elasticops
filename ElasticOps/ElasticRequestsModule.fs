﻿namespace ElasticOps



module ElasticRequestsModule =
    open System;
    open FSharp.Data;
    open System.IO;
    open FSharp.Data.JsonExtensions;

    let getUrl (baseUrl,ending : string) =
        (new Uri(new Uri(baseUrl),ending)).ToString()
        
        
    let getESResponse url =
        Http.RequestString url


    let output (response,args )=
       match args with
       | ">"::file::[] -> File.WriteAllText(file, response)
       | ">"::[] -> File.WriteAllText("lastResponse.txt", response)
       | _ -> Console.WriteLine  response

    let listNodes baseUrl args =
       let response = getUrl (baseUrl,"_nodes?pretty") |>  getESResponse  
       output(response,args)

    let indexSettings (url : string, index :string ) = 
        getUrl (url, (String.Format("{0}/_settings?pretty&flat_settings", index))) 
            |> getESResponse  
            |> Console.WriteLine 

    let indexStats (url : string, index :string , args : string list) = 
        let response = getUrl (url, (String.Format("{0}/_stats?pretty&flat_settings", index))) |> getESResponse  
        match args with 
            | "docs"::"count"::[] ->
                let pr = JsonValue.Parse(response)
                pr?_all?primaries?docs?count.AsString() |> Console.WriteLine
            | "docs"::tail ->
                let res = getUrl (url, (String.Format("{0}/_stats/docs?pretty&flat_settings", index))) |> getESResponse  
                output(res,tail)
            | _ -> output(response ,args)
        

    let pingES url =
        getESResponse url |> Console.WriteLine
        
    let getClsuterSettings (args : string list) =  
        getESResponse "http://localhost:9200/_cluster/settings?pretty&flat_settings"|> Console.WriteLine 

    let getMapping (url,index, _type, args : string list) =
        let response = getUrl(url,(String.Format("/{0}/{1}/_mapping?pretty&flat_settings", index,_type))) |> getESResponse  
        output(response,args)

    let getDoc (url,index, _type,id, args : string list) =
        let response = getUrl(url,(String.Format("/{0}/{1}/{2}?pretty", index,_type,id))) |> getESResponse  
        output(response,args)        