﻿namespace ElasticOps

module Shell =
    open System;
    open ElasticRequestsModule;

    let getCommand () = 
        List.ofSeq ((Console.ReadLine()).Trim().Split(' '))
    let help =
        Console.WriteLine "
Notation:
[<arg>] = optionl argument 
[arg] = required arg
> = output redirection to file

Default values:
baseUrl = localhost:9200
fileName = lastResponse.txt

Available commands:
[<baseUrl>] _nodes > [<fileName>]
[<baseUrl>] [index] _stats [<docs>] > [<fileName>]
[<baseUrl>] [index] _stats docs count
[<baseUrl>] [index] _settings 
[<baseUrl>] [index] [type] _mapping > [<fileName>]  
[<baseUrl>] [index] [type] [docId] > [<fileName>]   

[<baseUrl>] ping
clear
exit
                        "
        
    let rec commandLoop commands =
        let baseUrl = "http://localhost:9200"
        let nextLoop () =
            Console.Write "$"
            getCommand() |> commandLoop


        match commands with
            | url::"_nodes"::tail  -> 
                    listNodes url tail
                    nextLoop()
            | "_nodes"::tail ->
                    listNodes baseUrl tail
                    nextLoop()
            | url::index::"_settings"::[] ->
                indexSettings (url, index)
                nextLoop()
            | index::"_settings"::[] ->
                indexSettings (baseUrl,index)
                nextLoop()
            | url::index::"_stats"::tail ->
                indexStats (url, index,tail)
                nextLoop()
            | index::"_stats"::tail ->
                indexStats (baseUrl,index,tail)
                nextLoop()
            | url::index::_type::"_mapping"::tail ->
                getMapping(url,index,_type,tail)
                nextLoop()
            | index::_type::"_mapping"::tail ->
                getMapping(baseUrl,index,_type,tail)
                nextLoop()
            | url::index::_type::id::">"::tail ->
                getDoc(url,index,_type,id,">"::tail)
                nextLoop()
            | index::_type::id::">"::tail ->
                getDoc(baseUrl,index,_type,id,">"::tail)
                nextLoop()
            | url::index::_type::id::tail ->
                getDoc(url,index,_type,id,tail)
                nextLoop()
            | index::_type::id::tail ->
                getDoc(baseUrl,index,_type,id,tail)
                nextLoop()

            | url::"ping"::[] -> 
                pingES url
            | "ping"::[] ->
                pingES baseUrl
                nextLoop()
            | "help"::[] ->
                help
                nextLoop()
            | "_start"::[] -> nextLoop()
            | "clear"::[] -> 
                Console.Clear()
                nextLoop()
            | "exit"::tail -> ignore()
            | _ -> 
                    Console.WriteLine "Uknown command"
                    nextLoop()

